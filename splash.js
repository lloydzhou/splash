var Splash = function(canvas, duration, sprites, autostart){
  var w = window.innerWidth, h = window.innerHeight, t1 = w/h, t2 = 720/1280, k1 = w/720, k2 = h/1280, f = t1>t2?t1>1?1:2:3, k = f==2?k1:k2;
  var time = function(){return new Date().getTime() / 1000;}, ctx = canvas.getContext("2d")
  , raf = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || 
      function( callback ){window.setTimeout(callback, 1000 / 60);}
  , splash = {canvas: canvas, ctx: ctx, begin: time(), end: duration, sprites: sprites || []}
  , draw = function(){
      splash.current = time() - splash.begin
      if (splash.end && splash.current >= splash.end) return 0;
      canvas.width = f==3 ? w : k*720
      canvas.height = f==2 ? h : k*1280
      ctx.setTransform(1,0,0,1,0,0)
      ctx.clearRect(0, 0, splash.canvas.width, splash.canvas.height);
      var m = splash.getTransform()
      splash.sprites.forEach(function(s){
        CanvasRenderingContext2D.prototype.setTransform.apply(splash.ctx, m)
        s.draw()
      })
      raf(draw)
    }
  splash.getTransform = function(){
    splash._t = [k, 0, 0, k, f==3?(w-h*t2)/2:0, f==2?(h-w/t2)/2:0]
    return splash._t
  }
  splash.start = function(){draw()}
  splash.sprite = function(img, start, delay, duration){
    var s = new Sprite(splash, img, start, delay, duration)
    splash.sprites.push(s)
    return s
  }
  autostart && splash.start()
  return splash
}
var Sprite = function(splash, img, start, delay, duration){
  var sprite = {
    splash: splash, img: img, _delay: delay || 0, _duration: duration, angle:0,
    _start: start || [1,0,0,1,0,0], _m: [1,0,0,1,start && start[4] || 0,start && start[5] || 0], _stop:false,
  }, _ = function(m,n){
    return [m[0]*n[0]+m[2]*n[1], m[1]*n[0]+m[3]*n[1], 
            m[0]*n[2]+m[2]*n[3], m[1]*n[2]+m[3]*n[3], 
            m[0]*n[4]+m[2]*n[5]+m[4], m[1]*n[4]+m[3]*n[5]+m[5]]
  }, a2m = function(a){var c=Math.cos(a),s=Math.sin(a);return [c,-s,s,c,0,0]}
  sprite.end = function(){
      return _(sprite._m, a2m(sprite.angle))
  }
  sprite.next = function(duration){
    return sprite.stop().splash.sprite(sprite.img, sprite.end(), sprite._delay+sprite._duration, duration)
  }
  sprite.getTransform = function(t){
    var de = sprite._delay, du = sprite._duration, k = du==0?1:(t - de)/du, m=sprite._m;
    k = k > 1 ? 1 : sprite._ease?sprite._ease(k):k
    var m = sprite._start.map(function(n, i){return (m[i]-n) * k + n})
    return sprite.angle ? _(m, a2m(sprite.angle*k)) : m
  }
  sprite.transform = function(n, isStart){
    isStart?sprite._start = _(sprite._start, n):sprite._m = _(sprite._m, n)
    return sprite
  }
  sprite.rotate = function(r, isStart){
    isStart?sprite._start=_(sprite._start,a2m(r)):sprite.angle+=r;
    return sprite
  }
  sprite.scale = function(sx,sy, isStart){
    return sprite.transform([sx,0,0,sy||sx,0,0], isStart)
  }
  sprite.move = sprite.translate = function(x,y,isStart){
    return sprite.transform([1,0,0,1,x,y],isStart)
  }
  sprite.start = function(x,y){ sprite._start[4] = x;sprite._start[5] = y;return sprite.moveTo(x,y)}
  sprite.moveTo = function(x,y){ sprite._m[4] = x;sprite._m[5] = y;return sprite}
  sprite.setTransform = function(m){sprite._m=m;return sprite}
  sprite.delay = function(d){sprite._delay=d;return sprite}
  sprite.duration = function(d){sprite._duration=d;return sprite}
  sprite.stop = function(){sprite._stop=true;return sprite}
  sprite.ease = function(e){if(!window.Ease){console.log('ease function not support.')}else sprite._ease=new Ease(e);return sprite}
  sprite.draw = function(){
    if (sprite.splash.current < sprite._delay) return
    else if (sprite._stop && sprite.splash.current > sprite._delay + sprite._duration){
        sprite.onend&&sprite.onend(sprite)
        return sprite.onend=null;
    }else if (sprite.onstart) {
        sprite.onstart(sprite)
        sprite.onstart = null;
    }
    CanvasRenderingContext2D.prototype.transform.apply(sprite.splash.ctx, sprite.getTransform(sprite.splash.current))
    this.splash.ctx.drawImage(sprite.img, -sprite.img.naturalWidth/2, -sprite.img.naturalHeight/2)
  }
  return sprite
}

